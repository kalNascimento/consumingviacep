package com.app.models;

public class Address {
    
    String cep;
    String logradouro;

    public Address() {
        // empty Constructor
    }

    public Address(String cep, String logradouro) {
        this.cep = cep;
        this.logradouro = logradouro;
    }

    public String getCep() {
        return this.cep;
    }

    public String getLogradouro() {
        return this.logradouro;
    }
}