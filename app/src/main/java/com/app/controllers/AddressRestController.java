package com.app.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.app.models.Address;

@RestController
@RequestMapping("/cep")
public class AddressRestController {

    @GetMapping("/hello")
    public String getHelloWorld() {
        return "Hello World!!";
    }

    @GetMapping(value = "/{cep}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Address> viaCep(@PathVariable String cep) {
        RestTemplate json = new RestTemplate();
        Address address = json.getForObject("https://viacep.com.br/ws/"+ cep +"/json/", Address.class);

        if (address.getCep() == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(address, HttpStatus.OK);
    }
}
